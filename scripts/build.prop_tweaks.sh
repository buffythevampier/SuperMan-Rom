#!/sbin/sh
# Written by Tkkg1994

aromabuildprop=/tmp/aroma/buildtweaks.prop
buildprop=/system/build.prop

mount /dev/block/platform/155a0000.ufs/by-name/SYSTEM /system

if grep -q item.1.25=1 /tmp/aroma/samsung.prop; then
	echo "Enable knox/tima since knox has been enabled"
	sed -i -- 's/ro.config.knox=v00/ro.config.knox=v30/g' $buildprop
fi

if grep -q finger=1 $aromabuildprop; then
	echo "# Fingerprint Tweak" >> $buildprop
	echo "fingerprint.unlock=1" >> $buildprop
fi

if grep -q user=1 $aromabuildprop; then
	echo "# Multiuser Tweaks" >> $buildprop
	echo "fw.max_users=30" >> $buildprop
	echo "fw.show_multiuserui=1" >> $buildprop
	echo "fw.show_hidden_users=1" >> $buildprop
	echo "fw.power_user_switcher=1" >> $buildprop
fi

if grep -q fix=1 $aromabuildprop; then
	echo "# Screen mirror fix" >> $buildprop
	echo "wlan.wfd.hdcp=disable" >> $buildprop
fi

if grep -q item.1.12=1 /tmp/aroma/google.prop; then
	echo "# Google Assistant" >> $buildprop
	echo "ro.opa.eligible_device=true" >> $buildprop
fi

if grep -q softkey=1 $aromabuildprop; then
	echo "# Samsung Softkey" >> $buildprop
	echo "qemu.hw.mainkeys=0" >> $buildprop
fi

exit 10

