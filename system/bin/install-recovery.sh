#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/platform/155a0000.ufs/by-name/RECOVERY:45746176:9a966987d0bac6ba1c46fb5cdcd515ea8d682cdc; then
  applypatch EMMC:/dev/block/platform/155a0000.ufs/by-name/BOOT:37578752:882f170e7bbb0076cfec80c1f95756c1b1308b8e EMMC:/dev/block/platform/155a0000.ufs/by-name/RECOVERY 9a966987d0bac6ba1c46fb5cdcd515ea8d682cdc 45746176 882f170e7bbb0076cfec80c1f95756c1b1308b8e:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
