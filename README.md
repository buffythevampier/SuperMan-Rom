# SuperMan-Rom
Reposity for SuperMan-Rom

- [XDA Thread](https://forum.xda-developers.com/s7-edge/development/rom-s7-edge-rom-v1-1-t3356699)

- [Telegram Chat (Invite)](https://t.me/joinchat/AAAAAD-RcLTV9gB6WHH4zg)

- [Telegram News Channel](https://t.me/SuperManRom)

# Features
- Merge sources on every new release by samsung [Kernel && Base (firmware tar)]
- Crazy amount of customisation in kernel (just look into synapse and you'll see)
- Debloated (customisable in aroma)
- Samsungs stock interface with some modifications (RC, Mods, etc)
- Increased overall smoothness and fluidity, and make the s7/s7e's even faster and a increase in overall battery life

Full list on [XDA Thread](https://forum.xda-developers.com/s7-edge/development/rom-s7-edge-rom-v1-1-t3356699)

## Requirements
- Unecrypted /data partion (Wipe, format data, type "yes", reboot recovery, check if /data can be mounted without errors)
- Twrp 3.0.2-3 and later
- Updated Bootloder and Modem
- Common sense to follow instructions

## Contribution
Spot an error/bug? Please fork then send a pull request!
Any contribution is welcome :)

### FAQ (Freaking Asked Questions)
Q: How to format /data?

A: TWRP >> Wipe >> Format Data >> Yes || Stock recovery >> Wipe /data, factory reset >> Yes

You will loose your photos and apps as /data is internal storage

Q: Why is the ram usage fairly high

A: Free Ram == waste ram on Linux based systems, but RAM usage will gradually decrease in future updates
